import math

from stringstore import StringStore
from utilities import print_error

STORE = StringStore()  # the string store data structure to store relation components in.


class Relation:
    """
    A relation between two entities with a label.
    The edges be directed or undirected.
    All strings are stored within a special trie-like tree structure.
    """

    @staticmethod
    def write_graph_dot(relations: list, filename: str, max_thickness: int = 8):
        """
        Writes a given list of relations to a file in .DOT graph format.
        :param relations: The list of relations.
        :param filename: The name of the file to write the relations in.
        :param max_thickness: Maximum value for edge thickness.
        """
        max_count = max(rel[1] for rel in relations) if len(relations) > 0 else 0

        try:
            with open(filename, 'w') as outfile:
                outfile.write('digraph {\n')  # write a directed graph
                for rel_tup in relations:
                    rel = rel_tup[0]
                    rel_dir = ''
                    if rel.bidirectional:
                        rel_dir = ', dir=both'
                    thickness = math.ceil((rel_tup[1] / max_count * max_thickness))
                    outfile.write('\t "{}" -> "{}" [ label="{}", penwidth={}{}];\n'.format(rel.source, rel.target, rel.name,
                                                                                                thickness, rel_dir))
                outfile.write('}')
        except IOError:
            print_error('Could not open file: ' + filename)

    @staticmethod
    def _clean(text: str) -> str:
        """
        Replaces double quotes with single ones and strips white spaces.
        :param text: The text to be cleaned.
        :return: The cleaned text.
        """
        return text.replace('"', "\'").strip()

    def __init__(self, source: str, target: str, name: str, bidirectional: bool = False, clean: bool = True):
        """
        Creates a new relation object.
        :param source: The source of the relation.
        :param target: The target of the relation.
        :param name: The name or label of the relation.
        :param bidirectional: True if the relation is bidirectional.
        :param clean: True if source and target should be cleaned.
        """
        if clean:
            source = Relation._clean(source)
            target = Relation._clean(target)

        self.source_node = STORE.add_string(source)
        self.target_node = STORE.add_string(target)
        self.name_node = STORE.add_string(name)
        self.bidirectional = bidirectional
        self.source_node.relations.add(self)

    @property
    def source(self) -> str:
        """
        Gets the source of the relation.
        :return: The source.
        """
        return self.source_node.string

    @source.setter
    def source(self, string: str) -> None:
        """
        Sets the source of the relation.
        :param string: The new source.
        """
        self.source_node.relations.remove(self)
        self.source_node = STORE.add_string(string)
        self.source_node.relations.add(self)

    @property
    def target(self) -> str:
        """
        Gets the target of the relation.
        :return: The target.
        """
        return self.target_node.string

    @target.setter
    def target(self, string: str) -> None:
        """
        Sets the target of the relation.
        :param string: The new target.
        """
        self.target_node = STORE.add_string(string)

    @property
    def name(self) -> str:
        """
        Gets the name of the relation.
        :return: The name.
        """
        return self.name_node.string

    @name.setter
    def name(self, string: str) -> None:
        """
        Sets the name of the relation.
        :param string: The new name.
        """
        self.name_node = STORE.add_string(string)

    def __str__(self):
        """
        Gets the string representation of the relation.
        :return: The string representation.
        """
        return self.name + ' (' + self.source + ', ' + self.target + ')'

    def __repr__(self):
        """
        Gets the string representation of the relation.
        :return: The string representation.
        """
        return self.__str__()

    def __eq__(self, other: 'Relation') -> bool:
        """
        Determines if two relations are equal.
        :param other: The relation to compare two.
        :return: True if they are equal, False otherwise.
        """
        return type(self) == type(other) and self.source == other.source and self.target == other.target and self.name == other.name and \
               self.bidirectional == other.bidirectional

    def __hash__(self) -> int:
        """
        Computes the hash value of the relation.
        :return: The hash.
        """
        return hash(self.source) ^ hash(self.target) ^ hash(self.name) ^ hash(self.bidirectional)

