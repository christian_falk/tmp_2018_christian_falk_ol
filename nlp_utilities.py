import re
from typing import List, Iterable, Generator, Union, Sequence, Tuple
import random

import de_core_news_md
import tqdm
from gensim.models import KeyedVectors
from nltk.corpus import stopwords

from pattern import Pattern
from utilities import TokenTuple
from relation import Relation


# load the spaCy model and only use the tagger pipeline component
MODEL = de_core_news_md.load()
MODEL.pipeline = [MODEL.tagger]

_HYPHEN_PATTERN = re.compile(r'([A-Z][a-z]+-[A-Z][a-z]+) ([A-Z][a-z]+ [A-Z][a-z]+)')
_TITLE_PATTERN = re.compile(r'((?:[A-Z][a-z]+-)?[A-Z][a-z]+) \'(.+)\'')

STOPWORDS = set(stopwords.words('german')).union({'\n'})  # create a custom list of stop words


class Sentencizer(object):
    """
    Iterable interface for splitting POS-tagged text into sentences.
    """

    def __init__(self, source):
        """
        Creates a new sentencizer object.
        :param source:
        """
        self.source = source

    def __iter__(self):
        """
        Iterates over all sentences created by splitting.
        :return: The iterator.
        """
        temp = list()
        for doc in MODEL.pipe(self.source, n_threads=-1, batch_size=64):
            temp.clear()
            for token in [(str(tok), tok.tag_) for tok in doc if str(tok) != '\n']:
                if token[1] == '$.' and token[0] in {'!', '.'}:
                    temp.append(token)
                    yield [token[0] for token in merge_entities(temp, strict_mode=False)]
                    temp.clear()
                elif not is_stopword(token[0]):
                    temp.append(token)


class Matcher(object):
    """
    This class contains static matching function objects.
    Each of these functions takes a POS-tagged token as argument and returns true if there is a match.
    """
    capital_regex = re.compile(r'^Hauptstadt [A-Z][a-z]+$')  # regex matching for 'Hauptstadt <Name>'

    @staticmethod
    def founder_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token depicts foundership.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] in {'Gründer', 'Gründerin', 'Erfinder', 'Erfinderin'}

    @staticmethod
    def author_of(token: TokenTuple) -> bool:
        """
        Matches if the token depicts authorship.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] in {'Autor', 'Autorin'}

    @staticmethod
    def capital_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is 'Hauptstadt' followed by the capital's name.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return Matcher.capital_regex.match(token[0]) is not None

    @staticmethod
    def adjective_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is an adjective.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[1] in {'ADJA', 'ADJD'}

    @staticmethod
    def cap_article_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is a capitalized article.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[1] == 'ART' and token[0][0].isupper()

    @staticmethod
    def noun_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is a noun.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[1] in {'NE', 'NNE', 'NN'}

    @staticmethod
    def prop_noun_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is a proper noun.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return Matcher.noun_matcher(token)

    @staticmethod
    def is_matcher(token: TokenTuple) -> bool:
        """
        Matches exactly if the token is the string 'is'.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] == 'ist'

    @staticmethod
    def a_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is a indefinite article.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] in {'ein', 'eine'}

    @staticmethod
    def like_matcher(token: TokenTuple) -> bool:
        """
        Matches exactly if the token is the string 'wie'.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] == 'wie'

    @staticmethod
    def example_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token depicts an axample.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] in {'beispielsweise', 'etwa', 'z.B.'}

    @staticmethod
    def for_matcher(token: TokenTuple) -> bool:
        """
        Matches exactly if the token is the string 'zum'
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] == 'zum'

    @staticmethod
    def example_matcher_noun(token: TokenTuple) -> bool:
        """
        Matches exactly if the token is the string 'Beispiel'.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] == 'Beispiel'

    @staticmethod
    def comma_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is a comma.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] == ','

    @staticmethod
    def other_matcher(token: TokenTuple) -> bool:
        """
        Matches exactly if the token is the string 'andere'.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] == 'andere'

    @staticmethod
    def and_or_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token is a logic conjunction.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] in {'und', 'oder'}

    @staticmethod
    def chief_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token depicts leadership.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] in {'Präsident', 'Präsidentin', 'Chef', 'Chefin'}

    @staticmethod
    def of_matcher(token: TokenTuple) -> bool:
        """
        Matches if the token depicts possession.
        :param token: The token with its POS tag.
        :return: True if the token matches.
        """
        return token[0] in {'des', 'der', 'von'}


def is_stopword(token: str) -> bool:
    """
    Determines whether a token is a stop word or not.
    :param token: The token.
    :return: True if it is a stop word, False otherwise.
    """
    return token in STOPWORDS


def lemmatize_noun(token: str) -> str:
    """
    Custom pattern-based lemmatizer for German nouns.
    :param token: The token to be lemmatized.
    :return: The token itself or its lemma if one of the patterns could be applied.
    """
    if token.endswith('ädte'):
        token = token[:-4] + 'adt'
    elif token.endswith('änder'):
        token = token[:-5] + 'and'
    elif token.endswith('ächte'):
        token = token[:-5] + 'acht'
    elif token.endswith('ände'):
        token = token[:-4] + 'and'
    elif token.endswith('ändern'):
        token = token[:-6] + 'and'
    elif token.endswith('üsse'):
        token = token[:-4] + 'uss'
    elif token.endswith('ngen'):
        token = token[:-4] + 'ng'
    return token


def lemmatize_adjective(token: str) -> str:
    """
    Custom pattern-based lemmatizer for German adjectives.
    :param token: The token to be lemmatized.
    :return: The token itself or its lemma if one of the patterns could be applied.
    """
    if token.endswith('ischen'):
        token = token[:-6] + 'isch'
    elif token.endswith('ische'):
        token = token[:-5] + 'isch'
    elif token.endswith('schen'):
        token = token[:-5] + 'sch'
    return token


def split_token(token: str) -> Union[Sequence[str], str]:
    """
    Tries to split up tokens that were merged by the entity recognizer.
    :param token: The token to be further split up.
    :return: Either the token itself or a sequence of groups containing the single components after the split.
    """
    match = _TITLE_PATTERN.match(token)
    if match and len(match.groups()) > 1:
        return match.groups()

    match = _HYPHEN_PATTERN.match(token)
    if match and len(match.groups()) > 1:
        return match.groups()

    return token


def relative_cosine_similarity(w1: str, w2: str, word_vectors: KeyedVectors, top_n: int = 10) -> float:
    """
    Computes a symmetric version of the relative cosine similarity as the average of both relative cosine
    similarities between the two words, each in at first and second position.
    :param w1: The first word.
    :param w2: The second word.
    :param word_vectors: The word vectors for similarity computation.
    :param top_n: Parameter of the relative cosine similarity.
    :return: The symmetric relative cosine similarity.
    """
    cos = word_vectors.similarity(w1, w2)
    denom1 = sum(t[1] for t in word_vectors.most_similar(positive=w1, topn=top_n))
    denom2 = sum(t[1] for t in word_vectors.most_similar(positive=w2, topn=top_n))
    return (cos / 2.) * (1. / denom1 + 1. / denom2)


def make_tagged_tokens(source: Iterable) -> Generator[List[TokenTuple], None, None]:
    """
    Takes raw text documents from the source and performs tokenization and POS tagging.
    Newlines are removed.
    :param source: The iterable source of documents.
    :yield: A list of (token, tag) tuples representing the current document.
    """
    for doc in tqdm.tqdm(MODEL.pipe(source, n_threads=-1, batch_size=64)):
        yield [(str(tok), tok.tag_) for tok in doc if str(tok) != '\n']


# ART N ist <ein|eine> N
is_a_pattern = Pattern([Matcher.cap_article_matcher, Matcher.prop_noun_matcher, Matcher.is_matcher, Matcher.a_matcher,
                        Matcher.noun_matcher],
                       first_pos=1, second_pos=4)

# N wie <beispielsweise|etwa> N
such_as_pattern = Pattern([Matcher.noun_matcher, Matcher.like_matcher, Matcher.example_matcher, Matcher.prop_noun_matcher], first_pos=3,
                          second_pos=0)

# N, wie <beispielsweise|etwa> N
such_as_pattern2 = Pattern([Matcher.noun_matcher, Matcher.comma_matcher, Matcher.like_matcher, Matcher.example_matcher,
                            Matcher.prop_noun_matcher],
                           first_pos=4, second_pos=0)

# N wie N, N <und|oder> N
such_as_pattern3 = Pattern([Matcher.noun_matcher, Matcher.like_matcher, Matcher.prop_noun_matcher, Matcher.comma_matcher,
                            Matcher.prop_noun_matcher, Matcher.and_or_matcher, Matcher.prop_noun_matcher],
                           first_pos=(2, 4, 6), second_pos=0)

# N wie zum Beispiel N
example_pattern = Pattern([Matcher.noun_matcher, Matcher.like_matcher, Matcher.for_matcher,
                           Matcher.example_matcher_noun, Matcher.prop_noun_matcher],
                          first_pos=4, second_pos=0)

# N, wie zum Beispiel N
example_pattern2 = Pattern([Matcher.noun_matcher, Matcher.comma_matcher, Matcher.like_matcher, Matcher.for_matcher,
                            Matcher.example_matcher_noun, Matcher.prop_noun_matcher],
                           first_pos=5, second_pos=0)

# N <und|oder> andere N
and_other_pattern = Pattern([Matcher.prop_noun_matcher, Matcher.and_or_matcher, Matcher.other_matcher, Matcher.noun_matcher], first_pos=0,
                            second_pos=3)

# N, <Gründer|Erfinder> <des|der|von> N
founder_pattern = Pattern([Matcher.prop_noun_matcher, Matcher.comma_matcher, Matcher.founder_matcher, Matcher.of_matcher,
                           Matcher.prop_noun_matcher], first_pos=0, second_pos=4)

# N, <Autor> <des|der|von> N
author_pattern = Pattern([Matcher.prop_noun_matcher, Matcher.comma_matcher, Matcher.author_of, Matcher.of_matcher,
                          Matcher.prop_noun_matcher], first_pos=0, second_pos=4)

# <ADJ> Hauptstadt N
capital_pattern = Pattern([Matcher.adjective_matcher, Matcher.capital_matcher], first_pos=1, second_pos=0)

# N, <Chef|Präsident> von N
chief_pattern = Pattern([Matcher.prop_noun_matcher, Matcher.comma_matcher, Matcher.chief_matcher, Matcher.of_matcher,
                         Matcher.prop_noun_matcher], first_pos=0, second_pos=4)


def merge_entities(tokens: List[TokenTuple], strict_mode: bool = True, quote_threshold: int = 6) -> List[TokenTuple]:
    """
    Performs a merge of continuous tokens representing one entity.
    Merged entity tokens get the POS tag of the former first token.
    :param tokens: A list token (token, tag) tuples.
    :param strict_mode: Determines whether strict mode should be used.
                        In this mode only tokens with the exact same POS tag are merged.
    :param quote_threshold: Threshold value for the length of quotes that are recognized as entities
                        instead of quoted citations.
    :return: A list of (token, tag) tuples where entities have been merged.
    """
    result = list()
    quote_threshold += 2
    merge = list()
    quote_buffer = list()  # rollback buffer, if a too long quote has been recognized
    i = 0
    while i < len(tokens):
        token = tokens[i]
        starting_quote = False
        in_quote = False
        enter_quote = False
        trivial_quote = False
        broken_quote = False
        if Matcher.prop_noun_matcher(token) or token[0] == '"':  # search for the beginning of an entity
            if token[0] == '"':
                starting_quote = True
                in_quote = True
                noun_type = None
                enter_quote = True
            else:
                noun_type = token[1]  # a proper noun has been found as an entity
            k = i + 1
            merge.clear()
            quote_buffer.clear()

            if token[0] != '"':
                # merge.append(token[0])
                merge.append(token)

            if k < len(tokens):
                if starting_quote and not Matcher.prop_noun_matcher(tokens[k]):
                    trivial_quote = True
                elif starting_quote:
                    noun_type = tokens[k][1]

            while k < len(tokens) and (((strict_mode and tokens[k][1] == noun_type) or (not strict_mode
                                                                                        and tokens[k][1] in {'NN', 'NE', 'NNE'})) or
                                       tokens[k][0] == '-' or tokens[k][0] == '"' or in_quote):
                # appendix = tokens[k][0]
                appendix = tokens[k]

                # if in_quote:
                quote_buffer.append(tokens[k])

                if len(quote_buffer) > quote_threshold:
                    broken_quote = True

                # fill the POS tag delayed, e.g. if the phrase started with a quote
                # as a heuristic, the tag of the first token in the quote is used
                if noun_type is None:
                    noun_type = tokens[k][1]

                if in_quote and tokens[k][0] == '"':  # found the end of a quote
                    in_quote = False

                    if broken_quote:
                        # result += quote_buffer
                        merge.append(tokens[k])
                        result += merge
                        merge.clear()
                        k = k + 1
                        break

                    if len(merge) > 0:  # handle double quotes
                        # merge[-1][0] = ''.join([merge[-1][0], '"'])
                        merge = merge[:-1] + [(''.join([merge[-1][0], '"']), merge[-1][1])]
                    if trivial_quote:
                        k = k + 1
                        break
                elif (not in_quote) and tokens[k][0] == '"':  # found the beginning of a quote
                    in_quote = True
                    enter_quote = True
                else:
                    # either within or outside of a quote
                    if enter_quote and tokens[k][0] != '"':
                        # the first word of a quote has been read, so quote it
                        # appendix = ''.join(['"', appendix])
                        appendix = (''.join(['"', appendix[0]]), appendix[1])
                        enter_quote = False
                        # quote_buffer.insert(0, ('"', '$.'))
                    merge.append(appendix)
                k = k + 1

            if len(merge) > 0:  # could be zero in case of a broken quote, then the content is appended above
                result.append((' '.join([tok[0] for tok in merge]).replace(' - ', '-'), noun_type))
            i = k
        else:
            if token[1] != 'SP':
                # don't add whitespace-only tokens
                result.append(token)
            i = i + 1

    return result


def validate_relations(relations: List[Relation]):
    """
    Asks the user on the command line to enter the number of samples.
    Furthermore samples are chosen randomly and assessed by the user.
    The resulting precision score is printed to the console.
    :param relations: The relations to be evaluated.
    :return: None
    """
    entered_num = False
    min_samp = 1
    max_samp = len(relations)
    samples_count = 0
    correct_count = 0
    valid_answers = {'y': True, 'Y': True, 'n': False, 'N': False}
    yes_answers = {key: valid_answers[key] for key in ['y', 'Y'] if key in valid_answers}
    while not entered_num:
        try:
            samples_count = int(input('Number of samples to test: ').strip())
            if min_samp <= samples_count <= max_samp or samples_count == 0:
                entered_num = True
        except ValueError:
            pass
        if not entered_num:
            print('Please provide a natural number between {} and {}, 0 to abort'.format(min_samp, max_samp))

    if samples_count == 0:
        return

    for sample_index in random.sample(range(len(relations)), samples_count):
        entered_choice = False
        while not entered_choice:
            print(relations[sample_index])
            correct = input('Is this relation correct? [y/n]: ').strip()
            if correct in valid_answers:
                if correct in yes_answers:
                    correct_count += 1
                entered_choice = True

            if not entered_choice:
                print('Please enter your answer [y/n]')

    print('Result: {}% correct relations'.format(100. * correct_count / float(samples_count)))


def map_relations(extractions: Iterable[Tuple[TokenTuple]], rel_label: str) -> Iterable:
    """
    Maps textual pattern extractions to relations.
    :param extractions: Iterable over pairs of (str, tag) tuples representing the extractions
    :param rel_label: The label of the relation.
    :return: An iterable over the relations.
    """
    return map(lambda r: Relation(r[0][0], r[1][0], rel_label), extractions)
