import argparse
import gc
import logging
import os
from itertools import chain

import nltk
import psutil
import tqdm
from gensim.models import KeyedVectors
from gensim.models import Word2Vec

import nlp_utilities
from nationality import NationalityResolver
from nlp_utilities import and_other_pattern
from nlp_utilities import author_pattern
from nlp_utilities import capital_pattern
from nlp_utilities import chief_pattern
from nlp_utilities import founder_pattern
from nlp_utilities import lemmatize_noun
from nlp_utilities import make_tagged_tokens
from nlp_utilities import merge_entities
from nlp_utilities import relative_cosine_similarity as rcs
from nlp_utilities import such_as_pattern
from nlp_utilities import such_as_pattern2
from nlp_utilities import such_as_pattern3
from nlp_utilities import validate_relations
from nlp_utilities import map_relations
from utilities import print_error
from pattern import Pattern
from relation import Relation
from spiegel_corpus import SpiegelCorpus
from stringstore import StringStore

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def train_word_embedding(corpus, store_model: str):
    """
    Trains the word2vec word embedding on the given corpus.
    :param corpus: An iterable over the sentences of the corpus.
    :param store_model: If not None, the model will be stored in this file.
    """
    sentencizer = nlp_utilities.Sentencizer(corpus)

    logging.info('Starting training of word embedding')
    model = Word2Vec(sentences=sentencizer, size=400, window=8, min_count=50, sg=1,
                     workers=1, iter=5, seed=0, max_vocab_size=10000000)

    logging.info('Freeing memory after learning the word embedding')

    process = psutil.Process(os.getpid())
    cur_mem = process.memory_info().rss

    # free memory
    word_vectors = model.wv
    del model
    word_vectors.init_sims(replace=True)
    gc.collect()

    freed_mem = cur_mem - process.memory_info().rss
    if freed_mem > 0:
        logging.info('Freed {} bytes of memory after training'.format(freed_mem))
    else:
        logging.info('Unable to free memory after training')

    if store_model:
        logging.info('Saving word embedding to file: ' + store_model)
        try:
            word_vectors.save(store_model)
        except IOError:
            print_error('Could not write to file: ' + store_model)


def main(args):
    """
    The main routine of the ontology learning system.
    :param args: The command line arguments
    """
    word_vectors = None  # prevent reference before assignment warning
    corpus = SpiegelCorpus(suffix='.xml')
    if args.load_model:  # model is loaded from file
        logging.info('Loading model from file: ' + args.load_model)
        try:
            word_vectors = KeyedVectors.load(args.load_model)
        except FileNotFoundError:
            print_error('The model file could not be found: ' + args.load_model)
            exit(-1)
    else:  # a new model is generated
        train_word_embedding(corpus, args.store_model)

    relations = list()
    resolver = NationalityResolver()

    patterns = tuple([such_as_pattern, such_as_pattern2, such_as_pattern3, and_other_pattern, capital_pattern, chief_pattern,
                      author_pattern, founder_pattern])

    logging.info('Matching patterns in corpus')

    # initialize counters for statistics
    chief_rel_count = 0
    cap_rel_count = 0
    founder_rel_count = 0
    author_rel_count = 0
    isa_rel_count = 0

    # search for patterns in corpus
    for article in make_tagged_tokens(corpus):
        article = merge_entities(article, strict_mode=False)
        result_dict = Pattern.search_all(patterns, article)

        # add founder-of relations
        founder_results = result_dict[hash(founder_pattern)]
        founder_rel_count += len(founder_results)
        relations += map_relations(founder_results, 'Gründer von')

        # add author-of relations
        author_results = result_dict[hash(author_pattern)]
        author_rel_count += len(author_results)
        relations += map_relations(author_results, 'Author von')

        # add chief-of relations
        chief_results = result_dict[hash(chief_pattern)]
        chief_rel_count += len(chief_results)
        relations += map_relations(chief_results, 'Chef von')

        # add hyponym relations
        is_a_results = result_dict[hash(such_as_pattern)] + result_dict[hash(such_as_pattern2)] + result_dict[hash(such_as_pattern3)] + \
                       result_dict[hash(and_other_pattern)]
        isa_rel_count += len(is_a_results)
        relations += map_relations(is_a_results, 'gehört zu')

        # add capital-of relations
        get_country = resolver.get_country
        cap_results = result_dict[hash(capital_pattern)]
        rels_without_cap = len(relations)
        relations += map(lambda p: Relation(p[0], p[1].split()[1], 'Hauptstadt'),  # create relations from (country, capital) tuples
                         filter(lambda p: p[0],  # filter out tuples that have no country
                                map(lambda res: (get_country(nlp_utilities.lemmatize_adjective(res[1][0])), res[0][0]), cap_results)))
        cap_rel_count += len(relations) - rels_without_cap

    # log statistics
    logging.info('Extracted {} relations from {} articles'.format(len(relations), corpus.articles_count))
    logging.info('\tis-a: {}'.format(isa_rel_count))
    logging.info('\tcapital-of: {}'.format(cap_rel_count))
    logging.info('\tchief-of: {}'.format(chief_rel_count))
    logging.info('\tauthor-of: {}'.format(author_rel_count))
    logging.info('\tfounder-of: {}'.format(founder_rel_count))

    # add subclass-of relations
    logging.info('Searching for subclass relations between entities')
    rels_without_subclasses = len(relations)
    relations += map(lambda p: Relation(p[0], p[1], 'subclass-of'), StringStore().get_compounds())
    logging.info('Extracted {} subclass relations'.format(len(relations) - rels_without_subclasses))

    logging.info('Refining relations')
    refined_relations = 0

    for rel in relations:
        target = rel.target
        target_split = nlp_utilities.split_token(target)

        # check whether the target has been split up
        if target_split != target:
            rel.target = target_split[1]

            # appending while iterating is fine in this case and prevents memory overhead
            relations.append(Relation(target_split[1], target_split[0], 'gehört zu'))
            refined_relations += 1

    logging.info('Refined relations and gained {} new relations'.format(refined_relations))

    # search for noun pairs where the first element is potentially an inflected form of the second one
    noun_lemma_mapping = dict(set(StringStore().pairs_with_ending(['en', 'n', 's'])))

    logging.info('Found {} potentially equal nouns'.format(len(noun_lemma_mapping)))

    # replace sources and targets in all relations with its lemma form
    relations = list(map(lambda r: Relation(lemmatize_noun(noun_lemma_mapping[r.source] if r.source in noun_lemma_mapping else r.source),
                                            lemmatize_noun(noun_lemma_mapping[r.target] if r.target in noun_lemma_mapping else r.target),
                                            r.name, bidirectional=r.bidirectional), relations))

    noun_lemma_mapping.clear()

    # extract all entities from the relations as existing source or target
    entities = list(set(chain.from_iterable(map(lambda r: (r.source, r.target), relations))))

    logging.info('Searching for synonyms')

    # search for synonyms
    synonyms = 0
    vocab = word_vectors.vocab
    similarity = word_vectors.similarity
    rank = word_vectors.rank
    triangle_matrix_size = len(entities) * len(entities) / 2.0 - len(entities)
    for (ent1, ent2) in (filter(lambda p: p[0] in vocab and p[1] in vocab and similarity(p[0], p[1]) > 0.8
                                          and rcs(p[0], p[1], word_vectors, top_n=3) > 0.35,
                                tqdm.tqdm(
                                    ((ent1, ent2) for (i, ent1) in enumerate(entities) for (j, ent2) in enumerate(entities)
                                     if i < j and ent1 != ent2),
                                    total=triangle_matrix_size)
                                )):
        msg = ' '.join([str(similarity(ent1, ent2)), str(Relation(ent1, ent2, 'Synonym', bidirectional=True)),
                        str(rcs(ent1, ent2, word_vectors, top_n=3))])
        logging.info(msg)
        relations.append(Relation(ent1, ent2, 'Synonym', bidirectional=True))
        synonyms += 1

    logging.info('Found {} synonym relations'.format(synonyms))

    store = StringStore()

    deriv_hyponyms = 0

    for (ent1, ent2) in (filter(lambda p: p[0] in vocab and p[1] in vocab and similarity(p[0], p[1]) > 0.85,
                                tqdm.tqdm(
                                    ((ent1, ent2) for (i, ent1) in enumerate(entities) for (j, ent2) in enumerate(entities)
                                     if i < j and ent1 != ent2),
                                    total=triangle_matrix_size)
                                )):

        if (rank(ent1, ent2) + rank(ent2, ent1)) / 2. < 2.:
            for rel in filter(lambda r: r.name == 'gehört zu', store.get_relations(ent1)):
                relations.append(Relation(ent2, rel.target, 'gehört zu'))
                deriv_hyponyms += 1

            for rel in filter(lambda r: r.name == 'gehört zu', store.get_relations(ent2)):
                relations.append(Relation(ent1, rel.target, 'gehört zu'))
                deriv_hyponyms += 1

    logging.info('Derived {} hyponym relations by co-hyponyms.'.format(deriv_hyponyms))

    # compute various performance measures
    logging.info('Got {} relations.'.format(len(relations)))
    entities = set(chain.from_iterable(map(lambda r: (r.source, r.target), relations)))
    logging.info('Got {} entities.'.format(len(entities)))
    logging.info('Got {} entities per article in average.'.format(len(entities) / float(corpus.articles_count)))
    logging.info('Got {} relations per article in average.'.format(len(relations) / float(corpus.articles_count)))
    rels_per_ent = sum(len(store.get_relations(ent)) for ent in entities) / float(len(entities))
    logging.info('Got {} outgoing relations per entity in average.'.format(rels_per_ent))

    if args.dot_file:

        # make a frequency distribution of the relations in order to assign certain confidence values
        frq = nltk.FreqDist(relations)
        Relation.write_graph_dot(frq.most_common(), args.dot_file)
    else:
        for rel in relations:
            print(rel)

    # optionally ask the user to assess the relations
    if args.validate:
        validate_relations(relations)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extract and validate an ontology from the Spiegel news corpus.')
    parser.add_argument('-v', '--validate', dest='validate', action='store_true', help='Validate the relations after extraction.')
    parser.add_argument('-l', '--load-model', metavar='<filename>', dest='load_model', default=None, help='If this is given, '
                                                                                                          'a model will be loaded from '
                                                                                                          'the file.')
    parser.add_argument('-s', '--store-model', metavar='<filename>', dest='store_model', default=None, help='If this is given, '
                                                                                                            'the model will be stored in '
                                                                                                            'this file.')
    parser.add_argument('-o', '--write-ontology', metavar='<filename>', dest='dot_file', default=None, help='If this is given, '
                                                                                                            'the ontology will be stored '
                                                                                                            'in this file. Otherwise, '
                                                                                                            'the relations will be '
                                                                                                            'printed to the console.')
    main(parser.parse_args())
