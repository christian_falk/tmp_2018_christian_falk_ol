import csv
from typing import Optional, List


class NationalityResolver(object):
    """
    Resolver interface for nationality adjectives and the corresponding country name.
    Mapping is done via a CSV file.
    """

    def __init__(self, filename: str = 'nationalities_german.csv', lower_key: bool = True):
        """
        Creates a new resolver object.
        :param filename: The name of the CSV file containing the mapping.
        :param lower_key: Determines whether the first letters of the keys are lowered or not.
        """
        self._dictionary = dict()

        # open the CSV file
        with open(filename, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='\"')
            for row in reader:
                if lower_key:
                    row[0] = row[0].lower()
                self._dictionary[row[0]] = row[1]

    def get_country(self, adjective: str) -> Optional[str]:
        """
        Get the country name for the given nationality adjective.
        :param adjective: The adjective.
        :return: The name of the corresponding country or None if no country was found.
        """
        if adjective in self._dictionary:
            return self._dictionary[adjective]
        else:
            return None

    def get_adjectives(self, country: str) -> List[str]:
        """
        Gets a list of nationality adjectives for the given country name.
        :param country: The name of the country.
        :return: A list of adjectives, possible empty.
        """
        return [adj for adj, cnt in self._dictionary.items() if cnt == country]
