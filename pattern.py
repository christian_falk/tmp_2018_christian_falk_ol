from typing import Union, List, Iterable

from utilities import TokenTuple


class Pattern:
    """
    A class for lexico-syntactic patterns used for
    relation extraction in unstructured text.
    """

    def __init__(self, matchers: list, first_pos: Union[int, Iterable[int]] = 0, second_pos: int = 1):
        """
        Creates a new pattern object.
        :param matchers: A list of matchers applied in order for this pattern.
        :param first_pos: A number or list of numbers of positions determining the subject of the resulting relation.
        :param second_pos: A position determining the object of the resulting relation.
        """
        self._matchers = matchers
        self._window_size = len(matchers)
        self._first_pos = first_pos
        self._second_pos = second_pos

    def search(self, tokens: list, result_dict: dict = None) -> List[TokenTuple]:
        """
        Applies the pattern to a list of tokens.
        :param tokens: The list of tokens to be applied to.
        :param result_dict: If this is not None, the results are additionally written
                            to the dictionary with the hash of the pattern as key.
        :return: The result of the pattern as a list of pairs of tokens.
        """
        token_len = len(tokens)
        results = list()

        for (i, _) in enumerate(tokens):
            if i + self._window_size < token_len:

                # fill window
                hit = True
                for (k, matcher) in enumerate(self._matchers):
                    if not matcher(tokens[i + k]):
                        hit = False
                if hit:
                    if type(self._first_pos) == int:
                        results.append((tokens[i + self._first_pos], tokens[i + self._second_pos]))
                    else:
                        for first_pos in self._first_pos:
                            results.append((tokens[i + first_pos], tokens[i + self._second_pos]))
            else:
                break
        if result_dict is not None:
            result_dict[self.__hash__()] = results
        else:
            return results

    @staticmethod
    def search_all(patterns: Iterable['Pattern'], tokens: List[TokenTuple]) -> dict:
        """
        Applies multiple patterns to the same list of tokens.
        This implementation is especially designed for performance improvements compared to Patterns.search().
        :param patterns: A list of patterns to be applied.
        :param tokens: The list of tokens to search in.
        :return: A dictionary of results. The result of a specific patterns can be accessed by its hash value as key in the dictionary.
        """
        # prepare the dictionary where the results are stored
        results = dict({hash(p): list() for p in patterns})

        # store the functions to append results to the list locally
        append_functions = tuple(results[hash(p)].append for p in patterns)

        token_len = len(tokens)
        i = 0
        while i < token_len:
            for p_num, pat in enumerate(patterns):
                if i + pat._window_size < token_len:
                    hit = True
                    k = 0

                    # use local variables for another performance improvement
                    matchers = pat._matchers
                    num_matchers = len(matchers)
                    while k < num_matchers:
                        if not matchers[k](tokens[i + k]):
                            hit = False
                            break
                        k = k + 1
                    if hit:
                        if type(pat._first_pos) == int:
                            append_functions[p_num]((tokens[i + pat._first_pos], tokens[i + pat._second_pos]))
                        else:
                            for first_pos in pat._first_pos:
                                append_functions[p_num]((tokens[i + first_pos], tokens[i + pat._second_pos]))
            i = i + 1

        return results
