import logging
from typing import Tuple, Generator

# for shorter typing annotations
TokenTuple = Tuple[str, str]
StringGenerator = Generator[str, None, None]


def print_error(msg):
    """
    Prints an error message to the console and logs it.
    :param msg: The message to be printed.
    """
    logging.error(msg)
    print('ERROR: ', msg)


def quote(string: str) -> str:
    """
    Surrounds the given string with quotation marks.
    :param string:
    :return:
    """
    return ''.join(['"', string, '"'])