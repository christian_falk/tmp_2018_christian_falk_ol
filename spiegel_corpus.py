import os
import re
import logging

try:
    import xml.etree.cElementTree as ET  # prefer faster C-implementation
except ImportError:
    import xml.etree.ElementTree as ET  # fallback implementation

from tqdm import tqdm


class SpiegelCorpus:
    """
    Wrapper class for the Spiegel Online news corpus.
    """

    def __init__(self, path: str = 'DerSpiegel', suffix: str = '.xml') -> None:
        """
        Creates a new corpus object.
        :param path: The path where the corpus files are stored.
        :param suffix: The suffix of the corpus files' names.
        """
        self.corpus_path = path
        self.articles_count = 0
        self.suffix = suffix

    def simplify_xml(self, suffix: str = '_new') -> None:
        """
        Reads the articles of the corpus in the original XML format
        and exports them in a simpler format.
        :param suffix: The suffix to append to the filename for the new files.
        """
        for filename in os.listdir(self.corpus_path):
            if filename.endswith(self.suffix):
                logging.info('Simplifying file: ' + filename)
                fullname = os.path.join(self.corpus_path, filename)
                tree = ET.parse(fullname)
                root = ET.Element('articles')

                for article in tqdm(tree.getroot().findall('artikel')):
                    content_node = article.find('inhalt')
                    article_text = '\n'.join([para.text for para in content_node.find('text').findall('absatz') if para.text is not None])
                    article_node = ET.SubElement(root, 'article')
                    article_node.text = re.sub(r'[\n ]{2,}', '\n', article_text)

                with open(fullname + suffix, 'w') as outfile:
                    ET.ElementTree(root).write(outfile, encoding='unicode')

    def __iter__(self) -> None:
        """
        Generator that iterates over the articles of the corpus.
        :yield: The news articles of the corpus.
        """
        for filename in os.listdir(self.corpus_path):
            if filename.endswith(self.suffix):
                logging.info('Reading file: ' + filename)
                fullname = os.path.join(self.corpus_path, filename)
                parse_context = ET.iterparse(fullname, events={'end'})
                for _, node in (n for n in parse_context if n[1].tag == 'article' and n[1].text):
                    self.articles_count += 1
                    yield node.text

                    # increase memory efficiency and scalability while parsing
                    node.clear()
                    del node
