from itertools import chain
from typing import List, Optional, Tuple

from relation import *
from utilities import StringGenerator


class SSNode(object):
    """
    Class representing inner nodes in the trie-like data structure of the string store.
    """

    def __init__(self, character: chr, parent: Optional['SSNode'], is_active: bool = False):
        """
        Initializes the node.
        :param character: The character stored by this node.
        :param parent: The parent node of the new node or None for the root.
        :param is_active: True, if the string represented by this node is in the string store.
        """
        self.character_code = ord(character) if character != '' else 0
        self.children = dict()
        self.parent = parent
        self.is_active = is_active
        self.relations = set()

    def __sizeof__(self) -> int:
        """
        Computes the approximate size of the data structure.
        :return: The memory size.
        """
        return self.character.__sizeof__() + self.is_active.__sizeof__() + self.children.__sizeof__() \
               + sum([self.children[c].__sizeof__() for c in self.children])

    def pairs_with_ending(self, endings: List[str]) -> List[Tuple[str, str]]:
        """
        Computes all pairs of words in the data structure where one string
        consists of the other one and a given ending.
        :param endings: The list of acceptable endings.
        :return: A list of pairs of words.
        """
        pairs = list()

        if self.is_active:
            string = self.string
            pairs = [(string + ending, string) for ending in endings if self.has_string(ending)]
        return [pair for c in self.children for pair in self.children[c].pairs_with_ending(endings)] + pairs

    def get_compounds(self, root: 'SSNode') -> chain:  # use string to avoid unresolved reference
        """
        Heuristic for getting compound noun pairs.
        :param root: A reference to the root node of the tree.
        :return: Iterable chain of the compounds.
        """
        # local references for faster lookup
        string = self.string
        cap = SSNode._capitalize
        children = self.children.copy()
        return chain(((string[:-1] + sub, cap(sub)) for sub in self.get_strings('') if root.has_string(cap(sub))
                      and len(string) > 1 and ' ' not in string),
                     (compound for c in children for compound in children[c].get_compounds(root)))

    @staticmethod
    def _capitalize(string: str) -> str:
        """
        Make the first letter of the given string a capital letter.
        :param string: The string.
        :return: The capitalized version of the string.
        """
        if len(string) > 0:
            return string[0].upper() + string[1:]
        else:
            return string

    def add_string(self, string: str) -> 'SSNode':  # use string to avoid unresolved reference
        """
        Adds a string to the store starting with this node.
        :param string: The string to be added.
        :return: The node that contains the newly added (or already present) string.
        """
        starting_char = string[0]

        if len(string) == 1:
            if starting_char in self.children:
                self.children[starting_char].is_active = True
            else:
                self.children[starting_char] = SSNode(starting_char, self, True)
            return self.children[starting_char]

        elif starting_char in self.children:
            return self.children[starting_char].add_string(string[1:])
        else:
            new_node = SSNode(starting_char, self, False)
            self.children[starting_char] = new_node
            return new_node.add_string(string[1:])

    def has_string(self, string: str) -> bool:
        """
        Determines whether a string is present in the store starting with this node.
        :param string: The string to be checked.
        :return: True if it is in the store, False otherwise.
        """
        starting_char = string[0]
        if starting_char in self.children:
            if len(string) == 1:
                return self.children[starting_char].is_active
            else:
                return self.children[starting_char].has_string(string[1:])
        else:
            return False

    def get_relations(self, string: str) -> List['Relation']:
        """
        Computes a list of relations with a string as source.
        :param string: The string.
        :return: A list of all relations with the given string as source.
        """
        starting_char = string[0]
        if starting_char in self.children:
            if len(string) == 1:
                return self.children[starting_char].relations
            else:
                return self.children[starting_char].get_relations(string[1:])
        else:
            return list()

    def get_strings_list(self):
        """
        Gets a list of strings contained in the subnodes of the current node.
        :return: The list of strings.
        """
        if self.is_active:
            return [self.character + s for c in self.children for s in self.children[c].get_strings()] + [self.character]
        else:
            return [self.character + s for c in self.children for s in self.children[c].get_strings()]

    def get_strings(self, substring: str) -> StringGenerator:
        """
        Recursively generates all strings that are represented
        by this node and its subnodes.
        :param substring: The substring of the path to this node.
        :yields: A string in the subtree.
        """
        for c in self.children.copy():
            for s in self.children[c].get_strings(substring + self.character):
                yield s
        if self.is_active:
            yield substring + self.character

    @property
    def string(self) -> str:
        """
        Recursively reconstructs the string that is represented by the node.
        :return: The string.
        """
        string = ''
        node = self
        while node is not None:
            string = ''.join([node.character, string])
            node = node.parent
        return string

    @property
    def character(self) -> chr:
        """
        Gets the character stored in the node.
        :return: The character.
        """
        return chr(self.character_code) if self.character_code != 0 else ''


class StringStore:
    """
    A trie-like data structure for storing strings.
    """

    instance = None  # singleton instance

    def __init__(self) -> None:
        """
        Creates a new singleton instance if not already present.
        """
        if StringStore.instance is None:
            StringStore.instance = StringStore.__StringStore()

    def __getattr__(self, item):
        """
        Attribute delegate to the singleton instance.
        :param item: The item to get.
        :return: The corresponding attribute of the singleton instance.
        """
        return getattr(self.instance, item)

    def __contains__(self, item) -> bool:
        """
        Delegates the containment test to the singleton instance.
        :param item: The item to test.
        :return: True if the element is contained, False otherwise.
        """
        return self.instance.__contains__(item)

    def __sizeof__(self) -> int:
        """
        Computes the approximate memory size of the structure.
        :return: The memory size in bytes.
        """
        return self.instance.__sizeof__()

    def __iter__(self) -> StringGenerator:
        """
        Iterates over all strings in the data structure.
        :return:
        """
        return self.instance.__iter__()

    class __StringStore:
        """
        Inner class for the singleton implementation.
        """

        def __init__(self) -> None:
            """
            Creates a new store object and initializes the root node.
            """
            self.root = SSNode('', None)

        def __contains__(self, item) -> bool:
            """
            Containment test of the data structure.
            :param item: The string to be tested.
            :return: True if it is contained in the tree, False otherwise.
            """
            return self.root.has_string(item)

        def __sizeof__(self) -> int:
            """
            Approximately computes the memory size of the tree.
            :return: The size in bytes.
            """
            return self.root.__sizeof__()

        def __iter__(self) -> StringGenerator:
            """
            Iterates over all strings in the tree.
            :return: A corresponding generator.
            """
            return self.get_strings()

        def get_strings(self) -> StringGenerator:
            """
            Iterates over all strings in the tree.
            :return: A corresponding generator.
            """
            return self.root.get_strings('')

        def get_relations(self, string) -> List['Relation']:
            """
            Gets all relations having the given source.
            :param string: The source node string.
            :return: A list of relations, possibly empty if not found.
            """
            return self.root.get_relations(string)

        def add_string(self, string: str) -> SSNode:
            """
            Adds a string to the data structure.
            :param string: The string to be added.
            :return: The node where the string is stored.
            """
            if len(string) == 0:
                raise ValueError('Can not add empty string.')
            return self.root.add_string(string)

        def has_string(self, string: str) -> bool:
            """
            Checks whether a string is contained in the tree.
            :param string: The string to be tested.
            :return: True if it is contained in the tree, False otherwise.
            """
            if len(string) == 0:
                return False
            return self.root.has_string(string)

        def pairs_with_ending(self, endings: List[str]) -> List[Tuple[str, str]]:
            """
            Computes all pairs of words in the data structure where one string
            consists of the other one and a given ending.
            :param endings: The list of acceptable endings.
            :return: A list of pairs of words.
            """
            return self.root.pairs_with_ending(endings)

        def get_compounds(self) -> chain:
            """
            Heuristic for getting compound noun pairs contained in the tree.
            :return: Iterable chain of the compounds.
            """
            return self.root.get_compounds(self.root)
